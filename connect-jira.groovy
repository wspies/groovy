// author : Wouter Spies

// ----------------------------------
// This script connects to Jira, executes a filter and loops through the result
// ----------------------------------

import groovy.json.JsonSlurper
import groovy.json.JsonOutput

def env = System.getenv()

def user = env['jiraUsername']
def password = env['jiraPassword']
def url = env['jiraUrl']
def filter = "search?jql=status=merge&maxResults=10"

def authentication = user + ":" + password
def authenticationEncoded = authentication.getBytes().encodeBase64().toString()
def connection = new URL(url+filter).openConnection();

connection.setRequestProperty("Content-Type", "application/json")
connection.setRequestProperty("Authorization", "Basic " + authenticationEncoded);

def responseCode = connection.getResponseCode();

if(responseCode.equals(200)) {
  
    def responseText = connection.getInputStream().getText()
    def slurper = new JsonSlurper().parseText(responseText)
    def issues = slurper.issues.key
  
    print JsonOutput.prettyPrint(responseText)
    
    issues.each { issue-> println "${issue}"
              
    }
}    