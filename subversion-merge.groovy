// author : Wouter Spies

// ----------------------------------
// This script retrieves Subversion revision based a search in SVN log 
// merges these changes in the local workspace
// commits these changes 
// ----------------------------------

import groovy.json.JsonSlurper

def env = System.getenv()

def svnUrl = env['svnUrl'];   
def svnWorkSpace = env['WORKSPACE']
def svnUser = ''
def svnPassword = ''
def search = ''
def revisions = [];
def mergeAction = false;
def out = new StringBuilder()
def err = new StringBuilder()   

// via SVN log search for the log for a specific string and retrieve all revisions related to this string
// the search string would typically be an issue number from anykind of bugtracking system (e.g. Jira)
def svnCommand = "svn log --username " + svnUser " --password " + svnPassword + " --search=" + search + " " + svnUrl
result = executeCommand(svnCommand)    
      
// for each line returned by SVN Log check if the line has a revision number
// line should start with a r and have multiple numbers behind it
// add a valid revision number into the revisions array
result.toString().eachLine{
  if((match = it =~ /^r([0-9]+)(.*)(\\|)/)){
    revisions.add(match[0][1])
  } 
}
      
println 'revisions:' + revisions.reverse().toString();

//When merging revisions, start with the oldest one and then work back to the highest revision number
revisions.reverse().each { 

  revision-> println "merge/commit revision ${revision}"
  
  mergeAction = false

  // before a merge, we update the workspace to the latest version      
  svnCommand = "svn update " + svnWorkSpace
  executeCommand(svnCommand)
      
  // execute the SVN merge command
  svnCommand = "svn merge -c" + revision + " " + svnUrl + " " + svnWorkSpace
  result = executeCommand(svnCommand)
  
  //Check each line of the merge result for the first three characters
  result.toString().eachLine{
    //If the currentline of the result contains a C, it means conflict, abort the merge    
    if((match = it =~ /^\s{0,2}C/)){
      throw new Exception("Merge conflict")
      }
              
    // if the currentline, first character contains one of the characters: ADUGET its a valid merge
    if((match = it =~ /^[ADUGET]/)){
        mergeAction = true
        println 'valid merge action: '+ mergeAction
      }
      
    // if the merge went correctly, let commit the merge into SVN
    if(mergeAction){
      svnCommitMessage = "MERGE " + issue + " revision " + revision
      svnCommand = "svn commit "  + svnWorkSpace  + " -m" + '"' + svnCommitMessage + '"'   
      result= executeCommand(svnCommand)
      }
  }
}

def executeCommand(command) {
  
  out = new StringBuilder()
  err = new StringBuilder()
 
  println command
      
  commandProc = command.execute()
  commandProc.waitForProcessOutput(out, err)
  commandProc.waitForOrKill(100000000)
    
  println "-------------executeCommand start-----------------------------------------"
  println "output: $out"
  println $err
  println "-------------executeCommand end-----------------------------------------"

  //if there is an error executing the command, thow an error
  
  if(err.toString() != ""){
    throw new Exception("Error executing command")
    }
  
  return out
}
